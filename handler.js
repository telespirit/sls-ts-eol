'use strict';

module.exports.eolauth = async event => {
  let tempLocation = '';
  tempLocation = event.queryStringParameters.state||'http://telespirit.test/';
  tempLocation += 'admin.php?mod=tseolauth&code='+event.queryStringParameters.code||'';
  return {
    statusCode: 301,
    headers: {
      Location: tempLocation
    }
  };
};
